package pl.enteralt.core

object DefaultValues {

    const val STRING = ""

    const val INTEGER = -1

    const val INTEGER_ZERO = 0

    const val LONG = -1L

    const val LONG_ZERO = 0L

    const val DOUBLE = -1.0

    const val FLOAT = -1f

    const val FLOAT_ZERO = 0f

    const val BOOLEAN = false
}