package pl.enteralt.core.network

import java.io.IOException

class NetworkException(cause: IOException) : RuntimeException(cause)