package pl.enteralt.core.network

import java.io.InterruptedIOException

/**
 * Inherited from InterruptedIOException because:
 * 1. OkHttp call adapter accepts only IOException or it will crash
 * 2. OkHttp will not retry request if retry is enabled
 */
class OkHttpInterceptorWrappedThrowable(val wrapped: Throwable) : InterruptedIOException(wrapped.message)