package pl.enteralt.core

/**
 * Performs given [action] using provided [receiver] as `this`. If the [receiver]
 * is `null`, the [action] won't be called.
 *
 * Does not return result.
 */
inline fun <T, R> using(receiver: T?, action: T.() -> R) {
    receiver?.action()
}