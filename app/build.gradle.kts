plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
}

android {
    namespace = "pl.enteralt.hometestapp"
    compileSdk = 34

    defaultConfig {
        applicationId = "pl.enteralt.hometestapp"
        minSdk = 24
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.1"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {

    implementation(Dependencies.coreKtx)
    implementation(Dependencies.lifeCycleRuntimeKtx)
    implementation(Dependencies.activityCompose)
    implementation(platform(Dependencies.composeBom))
    implementation(Dependencies.composeUi)
    implementation(Dependencies.composeUiGraphics)
    implementation(Dependencies.composeUiToolingPreview)
    debugImplementation(Dependencies.compposeUiTooling)
    implementation(Dependencies.composeMaterial3)

    // Koin
    implementation(Dependencies.koinCompose)
    implementation(Dependencies.koinComposeNavigation)

    // OkHttp
    implementation(Dependencies.okHttp)
    implementation(Dependencies.okHttpLoggingInterceptor)

    // Retrofit
    implementation(Dependencies.retrofit)
    implementation(Dependencies.retrofitMoshi)
    implementation(Dependencies.retrofitGson)

    // Moshi
    implementation(Dependencies.moshi)
    kapt(Dependencies.moshiCodeGen)

    // Coroutines
    implementation(Dependencies.coroutines)

    // Splash screen
    implementation(Dependencies.splashScreen)

    // Coil
    implementation(Dependencies.coil)

    // Room
    implementation(Dependencies.room)
    implementation(Dependencies.roomCoroutines)
    kapt(Dependencies.roomCompiler)

    implementation(project(Modules.CORE))
}