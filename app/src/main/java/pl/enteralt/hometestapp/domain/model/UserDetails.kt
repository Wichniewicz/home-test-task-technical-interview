package pl.enteralt.hometestapp.domain.model

data class UserDetails(
    val login: String,
    val avatarUrl: String,
    val name: String?,
    val company: String?,
    val location: String?,
    val email: String?
)