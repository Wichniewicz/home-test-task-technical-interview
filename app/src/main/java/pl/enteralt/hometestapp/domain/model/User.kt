package pl.enteralt.hometestapp.domain.model

data class User(val login: String, val avatarUrl: String)