package pl.enteralt.hometestapp.domain.repository

import kotlinx.coroutines.flow.Flow
import pl.enteralt.hometestapp.domain.model.User
import pl.enteralt.hometestapp.domain.model.UserDetails
import pl.enteralt.hometestapp.infrastructure.data.model.remote.ResultState

interface UsersRepository {

    suspend fun getUserList(): Flow<ResultState<List<User>>>

    suspend fun getUserDetails(userLogin: String): Flow<ResultState<UserDetails>>
}