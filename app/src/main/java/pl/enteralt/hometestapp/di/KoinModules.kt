package pl.enteralt.hometestapp.di

class KoinModules {

    companion object {

        fun all() = listOf(
            AppModule.create(),
            NetworkModule.create(),
            DatabaseModule.create(),
            ViewModelModule.create(),
            UsersModule.create()
        )
    }
}