package pl.enteralt.hometestapp.di

import android.content.Context
import androidx.room.Room
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import pl.enteralt.hometestapp.data.db.MainDatabase

class DatabaseModule {

    companion object {

        private fun provideRoomDatabase(context: Context) =
            Room.databaseBuilder(context, MainDatabase::class.java, MainDatabase.FILENAME)
                .build()

        private fun provideUserDao(mainDatabase: MainDatabase) = mainDatabase.userDao()

        fun create() = module {
            single { provideRoomDatabase(context = androidContext()) }
            single { provideUserDao(mainDatabase = get()) }
        }
    }
}