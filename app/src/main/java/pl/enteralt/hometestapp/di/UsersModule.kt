package pl.enteralt.hometestapp.di

import org.koin.dsl.module
import pl.enteralt.hometestapp.data.backend.BackendRequestHandler
import pl.enteralt.hometestapp.data.datasource.LocalUsersDataSource
import pl.enteralt.hometestapp.data.datasource.RemoteUsersDataSource
import pl.enteralt.hometestapp.domain.repository.UsersRepository
import pl.enteralt.hometestapp.infrastructure.datasource.local.LocalUsersDataSourceImp
import pl.enteralt.hometestapp.infrastructure.datasource.local.UserDao
import pl.enteralt.hometestapp.infrastructure.datasource.remote.RemoteUsersDataSourceImp
import pl.enteralt.hometestapp.infrastructure.datasource.remote.api.UsersRetrofitService
import pl.enteralt.hometestapp.infrastructure.repository.UsersRepositoryImp
import retrofit2.Retrofit
import retrofit2.create

class UsersModule {

    companion object {

        private fun provideUsersRestrofitService(retrofit: Retrofit): UsersRetrofitService = retrofit.create()

        private fun provideRemoteUsersDataSource(retrofitService: UsersRetrofitService, requestHandler: BackendRequestHandler) =
            RemoteUsersDataSourceImp(retrofitService, requestHandler)

        private fun provideLocalUsersDataSource(userDao: UserDao) = LocalUsersDataSourceImp(userDao)

        fun create() = module {

            single { provideUsersRestrofitService(retrofit = get()) }
            single<RemoteUsersDataSource> { provideRemoteUsersDataSource(retrofitService = get(), requestHandler = get()) }
            single<LocalUsersDataSource> { provideLocalUsersDataSource(userDao = get()) }
            single<UsersRepository> {
                UsersRepositoryImp(
                    localUsersDataSource = get(),
                    remoteUsersDataSource = get(),
                    dispatchers = get()
                )
            }
        }
    }
}