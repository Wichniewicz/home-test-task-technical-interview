package pl.enteralt.hometestapp.di

import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module
import pl.enteralt.core.common.CoroutineDispatchers

class AppModule {

    companion object {

        private fun provideCoroutineDispatchers() = CoroutineDispatchers(
            default = Dispatchers.Default,
            io = Dispatchers.IO,
            main = Dispatchers.Main
        )

        fun create() = module {

            single { provideCoroutineDispatchers() }
        }
    }
}