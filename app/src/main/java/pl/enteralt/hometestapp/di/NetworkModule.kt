package pl.enteralt.hometestapp.di

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import pl.enteralt.hometestapp.data.backend.BackendRequestHandler
import pl.enteralt.hometestapp.data.backend.BackendResponseHandler
import pl.enteralt.hometestapp.data.backend.dto.ErrorResponseDto
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

class NetworkModule {

    companion object {

        private const val BASE_URL = "https://api.github.com/"
        private const val HTTP_TIMEOUT = 40L // seconds

        private fun provideLoggingInterceptor() = HttpLoggingInterceptor()
            .apply {
                level = HttpLoggingInterceptor.Level.BASIC
            }

        private fun provideOkHttpClient(interceptor: HttpLoggingInterceptor) = OkHttpClient().newBuilder()
            .apply {
                addInterceptor(interceptor)
                connectTimeout(HTTP_TIMEOUT, TimeUnit.SECONDS)
                readTimeout(HTTP_TIMEOUT, TimeUnit.SECONDS)
                writeTimeout(HTTP_TIMEOUT, TimeUnit.SECONDS)
            }
            .build()

        private fun provideMoshi() = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()

        private fun provideRetrofit(okHttpClient: OkHttpClient, moshi: Moshi) = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()

        private fun provideErrorBodyConverter(retrofit: Retrofit) =
            retrofit.responseBodyConverter<ErrorResponseDto>(ErrorResponseDto::class.java, emptyArray())

        fun create() = module {
            factory { provideLoggingInterceptor() }
            factory { provideOkHttpClient(interceptor = get()) }
            factory { provideMoshi() }
            single { provideRetrofit(okHttpClient = get(), moshi = get()) }
            factory { provideErrorBodyConverter(retrofit = get()) }
            factory { BackendResponseHandler(errorResponseConverter = get()) }
            factory { BackendRequestHandler(backendResponseHandler = get()) }
        }
    }
}