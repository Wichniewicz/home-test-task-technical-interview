package pl.enteralt.hometestapp.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import pl.enteralt.hometestapp.view.viewmodel.DetailsViewModel
import pl.enteralt.hometestapp.view.viewmodel.ListViewModel

class ViewModelModule {

    companion object {

        fun create() = module {

            viewModel { ListViewModel(usersRepository = get()) }
            viewModel { DetailsViewModel(userLogin = get(), usersRepository = get()) }
        }
    }
}