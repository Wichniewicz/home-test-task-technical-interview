package pl.enteralt.hometestapp.data.datasource

import pl.enteralt.hometestapp.infrastructure.data.model.local.*

interface LocalUsersDataSource {

    suspend fun saveUsers(userList: List<RoomUser>)

    suspend fun saveUserDetails(details: RoomUserDetails)

    suspend fun getUserList(): List<RoomUser>

    suspend fun getUserDetails(userLogin: String): RoomUserWithDetails?
}