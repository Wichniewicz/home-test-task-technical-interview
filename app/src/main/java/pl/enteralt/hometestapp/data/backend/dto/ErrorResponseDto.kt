package pl.enteralt.hometestapp.data.backend.dto

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ErrorResponseDto(val errorCode: Int, val message: String?, val title: String?)
