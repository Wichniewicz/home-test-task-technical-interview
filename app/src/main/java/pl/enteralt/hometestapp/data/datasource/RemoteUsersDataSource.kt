package pl.enteralt.hometestapp.data.datasource

import pl.enteralt.hometestapp.infrastructure.data.model.remote.*

interface RemoteUsersDataSource {

    suspend fun getUserList(): ResultState<List<ApiUser>>

    suspend fun getUserDetails(userLogin: String): ResultState<ApiUserDetails>
}