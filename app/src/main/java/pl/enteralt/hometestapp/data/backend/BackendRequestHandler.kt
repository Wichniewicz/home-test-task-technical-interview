package pl.enteralt.hometestapp.data.backend

import android.util.Log
import kotlinx.coroutines.CancellationException
import pl.enteralt.core.network.NetworkException
import pl.enteralt.core.network.OkHttpInterceptorWrappedThrowable
import pl.enteralt.hometestapp.infrastructure.data.model.remote.ResultState
import retrofit2.Response
import java.io.IOException

private const val TAG = "BackendRequestHandler"

class BackendRequestHandler(private val backendResponseHandler: BackendResponseHandler) {

    suspend fun <T : Any> handle(
        successfulResponseHandler: (response: Response<T>) -> ResultState<T> = { backendResponseHandler.successful(it) },
        unsuccessfulResponseHandler: (response: Response<T>) -> ResultState.Error = { backendResponseHandler.unsuccessful(it) },
        request: suspend () -> Response<T>
    ): ResultState<T> {

        val response = try {
            request.invoke()
        } catch (t: OkHttpInterceptorWrappedThrowable) {
            return ResultState.Error(t.wrapped)
        } catch (ioex: IOException) {
            return ResultState.Error(NetworkException(ioex))
        } catch (ex: CancellationException) {
            return ResultState.Error(ex)
        } catch (ex: Exception) {
            Log.w(TAG, "Unexpected exception during backend request", ex)
            return ResultState.Error(ex)
        }

        return if (response.isSuccessful) {
            successfulResponseHandler.invoke(response)
        } else {
            unsuccessfulResponseHandler.invoke(response)
        }
    }
}