package pl.enteralt.hometestapp.data.backend

/**
 * Special backend exception for handling clearer error codes based on HTTP status code
 */
class BackendException(private val httpStatusCode: Int, message: String?, private val errorCode: Int? = null) : RuntimeException(
    message
) {

    override fun toString() = "${super.toString()} {httpStatusCode: $httpStatusCode, errorCode: $errorCode}"
}