package pl.enteralt.hometestapp.data.backend

import okhttp3.ResponseBody
import pl.enteralt.core.DefaultValues
import pl.enteralt.hometestapp.data.backend.dto.ErrorResponseDto
import pl.enteralt.hometestapp.infrastructure.data.model.remote.ResultState
import retrofit2.Converter
import retrofit2.Response
import java.net.HttpURLConnection

class BackendResponseHandler(private val errorResponseConverter: Converter<ResponseBody, ErrorResponseDto>) {

    fun <T : Any> successful(response: Response<T>): ResultState<T> {

        require(response.isSuccessful) { "Response must be successful" }

        val body = response.body()
        return if (body == null) {
            if (response.code() == HttpURLConnection.HTTP_NO_CONTENT) {
                @Suppress("UNCHECKED_CAST")
                ResultState.Success(Unit) as ResultState<T>
            } else {
                ResultState.Error(IllegalStateException("Successful response body is null"))
            }
        } else {
            ResultState.Success(body)
        }
    }

    fun <T : Any> unsuccessful(response: Response<T>): ResultState.Error {

        require(!response.isSuccessful) { "Response cannot be successful" }

        val errorBody = response.errorBody()

        val backendException = if (errorBody == null) {
            BackendException(response.code(), response.message())
        } else {
            try {
                val backendResponse = errorResponseConverter.convert(errorBody)
                BackendException(
                    response.code(),
                    backendResponse?.message,
                    backendResponse?.errorCode ?: DefaultValues.INTEGER_ZERO
                )
            } catch (ex: Exception) {
                BackendException(response.code(), response.message())
            }
        }

        return ResultState.Error(backendException)
    }
}