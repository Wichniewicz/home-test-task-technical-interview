package pl.enteralt.hometestapp.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import pl.enteralt.hometestapp.infrastructure.data.model.local.RoomUser
import pl.enteralt.hometestapp.infrastructure.data.model.local.RoomUserDetails
import pl.enteralt.hometestapp.infrastructure.datasource.local.UserDao

@Database(entities = [RoomUser::class, RoomUserDetails::class], version = MainDatabase.VERSION)
abstract class MainDatabase : RoomDatabase() {

    companion object {

        const val VERSION = 1
        const val FILENAME = "pl.enteralt.hometestapp.main.db"
    }

    abstract fun userDao(): UserDao
}