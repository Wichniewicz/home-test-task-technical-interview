package pl.enteralt.hometestapp.infrastructure.data.model.local

import androidx.room.Embedded
import androidx.room.Relation

data class RoomUserWithDetails(
    @Embedded
    val user: RoomUser,
    @Relation(parentColumn = "login", entityColumn = "login")
    val details: RoomUserDetails?
)
