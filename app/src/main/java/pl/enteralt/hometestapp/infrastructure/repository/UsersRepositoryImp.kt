package pl.enteralt.hometestapp.infrastructure.repository

import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.withContext
import pl.enteralt.core.common.CoroutineDispatchers
import pl.enteralt.hometestapp.data.datasource.LocalUsersDataSource
import pl.enteralt.hometestapp.data.datasource.RemoteUsersDataSource
import pl.enteralt.hometestapp.domain.repository.UsersRepository
import pl.enteralt.hometestapp.infrastructure.data.mapping.ApiUsersMapper.toDbUserDetails
import pl.enteralt.hometestapp.infrastructure.data.mapping.ApiUsersMapper.toDbUserList
import pl.enteralt.hometestapp.infrastructure.data.mapping.ApiUsersMapper.toDomainUserDetails
import pl.enteralt.hometestapp.infrastructure.data.mapping.ApiUsersMapper.toDomainUserList
import pl.enteralt.hometestapp.infrastructure.data.model.remote.ResultState

class UsersRepositoryImp(
    private val localUsersDataSource: LocalUsersDataSource,
    private val remoteUsersDataSource: RemoteUsersDataSource,
    private val dispatchers: CoroutineDispatchers
) : UsersRepository {

    private suspend fun getUserListFromLocalDataSource() = localUsersDataSource.getUserList()

    override suspend fun getUserList() = channelFlow {
        withContext(dispatchers.io) {

            val roomUserList = getUserListFromLocalDataSource()
            if (roomUserList.isNotEmpty()) {
                send(ResultState.Success(roomUserList.toDomainUserList())) // sends cached user-list
            }

            when (val resultState = remoteUsersDataSource.getUserList()) {
                is ResultState.Error -> send(resultState) // TODO this should be handled more properly
                is ResultState.Success -> {
                    localUsersDataSource.saveUsers(resultState.data.toDbUserList())
                    send(ResultState.Success(localUsersDataSource.getUserList().toDomainUserList()))
                }
            }
        }
    }.catch { error -> emit(ResultState.Error(error)) }

    private suspend fun getUserDetailsFromLocalDataSource(userLogin: String) = localUsersDataSource.getUserDetails(userLogin)

    override suspend fun getUserDetails(userLogin: String) =
        channelFlow {

            withContext(dispatchers.io) {

                getUserDetailsFromLocalDataSource(userLogin)?.let { send(ResultState.Success(it.toDomainUserDetails())) } // sends cached user-details

                when (val resultState = remoteUsersDataSource.getUserDetails(userLogin)) {
                    is ResultState.Error -> send(resultState) // TODO this should be handled more properly
                    is ResultState.Success -> {
                        localUsersDataSource.saveUserDetails(resultState.data.toDbUserDetails())
                        val roomUserWithDetails = getUserDetailsFromLocalDataSource(userLogin)
                        if (roomUserWithDetails != null) {
                            send(ResultState.Success(roomUserWithDetails.toDomainUserDetails()))
                        } else {
                            send(ResultState.Error(Throwable("No user details found for this user")))
                        }
                    }
                }
            }
        }.catch { error -> emit(ResultState.Error(error)) }
}