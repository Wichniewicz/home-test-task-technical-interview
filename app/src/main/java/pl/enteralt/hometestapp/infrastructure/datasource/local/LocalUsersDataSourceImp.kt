package pl.enteralt.hometestapp.infrastructure.datasource.local

import pl.enteralt.hometestapp.data.datasource.LocalUsersDataSource
import pl.enteralt.hometestapp.infrastructure.data.model.local.RoomUser
import pl.enteralt.hometestapp.infrastructure.data.model.local.RoomUserDetails

class LocalUsersDataSourceImp(private val userDao: UserDao) : LocalUsersDataSource {

    override suspend fun saveUsers(userList: List<RoomUser>) = userDao.saveUsers(userList)

    override suspend fun saveUserDetails(details: RoomUserDetails) = userDao.saveUserDetails(details)

    override suspend fun getUserList() = userDao.getUserList()

    override suspend fun getUserDetails(userLogin: String) = userDao.getUserDetails(userLogin)
}