package pl.enteralt.hometestapp.infrastructure.data.model.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class RoomUser(

    @PrimaryKey
    val login: String,
    val avatarUrl: String
)