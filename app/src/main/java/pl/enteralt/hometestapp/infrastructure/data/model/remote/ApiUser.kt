package pl.enteralt.hometestapp.infrastructure.data.model.remote

import com.squareup.moshi.Json
import pl.enteralt.core.DefaultValues

data class ApiUser(
    val login: String = DefaultValues.STRING,
    @Json(name = "avatar_url")
    val avatarUrl: String = DefaultValues.STRING
)