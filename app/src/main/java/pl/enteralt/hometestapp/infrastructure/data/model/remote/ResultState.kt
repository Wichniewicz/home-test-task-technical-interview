package pl.enteralt.hometestapp.infrastructure.data.model.remote

sealed class ResultState<out T : Any> {

    data class Success<out T : Any>(val data: T) : ResultState<T>()

    data class Error(val cause: Throwable) : ResultState<Nothing>()
}