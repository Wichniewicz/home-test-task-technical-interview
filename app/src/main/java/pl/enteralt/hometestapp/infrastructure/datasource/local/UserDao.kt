package pl.enteralt.hometestapp.infrastructure.datasource.local

import androidx.room.*
import androidx.room.OnConflictStrategy.Companion.REPLACE
import pl.enteralt.hometestapp.infrastructure.data.model.local.*

@Dao
interface UserDao {

    @Insert(onConflict = REPLACE)
    suspend fun saveUsers(user: List<RoomUser>)

    @Insert(onConflict = REPLACE)
    suspend fun saveUserDetails(details: RoomUserDetails)

    @Query("SELECT * FROM user")
    suspend fun getUserList(): List<RoomUser>

    @Transaction
    @Query("SELECT * FROM user WHERE user.login = :userLogin")
    suspend fun getUserDetails(userLogin: String): RoomUserWithDetails?
}