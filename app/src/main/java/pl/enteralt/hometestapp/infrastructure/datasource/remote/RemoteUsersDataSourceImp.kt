package pl.enteralt.hometestapp.infrastructure.datasource.remote

import pl.enteralt.hometestapp.data.backend.BackendRequestHandler
import pl.enteralt.hometestapp.data.datasource.RemoteUsersDataSource
import pl.enteralt.hometestapp.infrastructure.datasource.remote.api.UsersRetrofitService

class RemoteUsersDataSourceImp(
    private val retrofitService: UsersRetrofitService,
    private val requestHandler: BackendRequestHandler
) : RemoteUsersDataSource {

    override suspend fun getUserList() = requestHandler.handle { retrofitService.getUserList() }

    override suspend fun getUserDetails(userLogin: String) = requestHandler.handle { retrofitService.getUserDetails(userLogin) }
}