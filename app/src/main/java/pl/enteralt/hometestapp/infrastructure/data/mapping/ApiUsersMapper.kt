package pl.enteralt.hometestapp.infrastructure.data.mapping

import pl.enteralt.hometestapp.domain.model.User
import pl.enteralt.hometestapp.domain.model.UserDetails
import pl.enteralt.hometestapp.infrastructure.data.model.local.*
import pl.enteralt.hometestapp.infrastructure.data.model.remote.ApiUser
import pl.enteralt.hometestapp.infrastructure.data.model.remote.ApiUserDetails

object ApiUsersMapper {

    //region API -> db
    private fun ApiUser.toDbUser() = RoomUser(login, avatarUrl)

    fun List<ApiUser>.toDbUserList() = map { it.toDbUser() }

    fun ApiUserDetails.toDbUserDetails() = RoomUserDetails(login, name, company, location, email)
    //endregion

    //region Db -> domain
    private fun RoomUser.toDomainUser() = User(login, avatarUrl)

    fun List<RoomUser>.toDomainUserList() = map { it.toDomainUser() }

    fun RoomUserWithDetails.toDomainUserDetails() =
        UserDetails(user.login, user.avatarUrl, details?.name, details?.company, details?.location, details?.email)
    //endregion
}