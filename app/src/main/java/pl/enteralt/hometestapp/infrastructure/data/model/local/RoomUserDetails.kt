package pl.enteralt.hometestapp.infrastructure.data.model.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_details")
data class RoomUserDetails(
    @PrimaryKey
    val login: String,
    val name: String?,
    val company: String?,
    val location: String?,
    val email: String?
)
