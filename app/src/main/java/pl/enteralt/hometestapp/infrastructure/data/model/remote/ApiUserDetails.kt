package pl.enteralt.hometestapp.infrastructure.data.model.remote

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import pl.enteralt.core.DefaultValues

@JsonClass(generateAdapter = true)
data class ApiUserDetails(
    val login: String = DefaultValues.STRING,
    @Json(name = "avatar_url")
    val avatarUrl: String = DefaultValues.STRING,
    val name: String? = DefaultValues.STRING,
    val company: String? = DefaultValues.STRING,
    val location: String? = DefaultValues.STRING,
    val email: String? = DefaultValues.STRING
)