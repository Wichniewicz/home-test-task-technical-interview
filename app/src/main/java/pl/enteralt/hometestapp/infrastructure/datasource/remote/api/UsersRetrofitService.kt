package pl.enteralt.hometestapp.infrastructure.datasource.remote.api

import pl.enteralt.hometestapp.infrastructure.data.model.remote.ApiUser
import pl.enteralt.hometestapp.infrastructure.data.model.remote.ApiUserDetails
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface UsersRetrofitService {

    @GET("users")
    suspend fun getUserList(): Response<List<ApiUser>>

    @GET("users/{userLogin}")
    suspend fun getUserDetails(@Path("userLogin") userLogin: String): Response<ApiUserDetails>
}