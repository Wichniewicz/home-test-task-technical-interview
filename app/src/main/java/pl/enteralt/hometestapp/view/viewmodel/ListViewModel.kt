package pl.enteralt.hometestapp.view.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import pl.enteralt.core.DefaultValues
import pl.enteralt.hometestapp.domain.repository.UsersRepository
import pl.enteralt.hometestapp.infrastructure.data.model.remote.ResultState
import pl.enteralt.hometestapp.view.model.ViewUser
import pl.enteralt.hometestapp.view.model.mapping.ViewUsersMapper.toViewUserList

private const val TAG = "ListViewModel"

class ListViewModel(private val usersRepository: UsersRepository) : ViewModel() {

    private val _uiState: MutableStateFlow<ListUiState> = MutableStateFlow(ListUiState.Loading)
    val uiState = _uiState.asStateFlow()

    private fun fetchUserList() = viewModelScope.launch {

        usersRepository.getUserList()
            .collectLatest { resultState ->
                when (resultState) {
                    is ResultState.Error -> {
                        Log.e(TAG, "fetchUserList -> Error state: ${resultState.cause.message}")
                        _uiState.value = ListUiState.Error(resultState.cause.message ?: DefaultValues.STRING)
                    }

                    is ResultState.Success -> {
                        _uiState.value = ListUiState.Success(resultState.data.toViewUserList())
                    }
                }
            }
    }

    init {
        fetchUserList()
    }
}

sealed interface ListUiState {
    data object Loading : ListUiState
    data class Success(val data: List<ViewUser>) : ListUiState
    data class Error(val message: String) : ListUiState
}