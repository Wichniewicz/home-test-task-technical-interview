package pl.enteralt.hometestapp.view.screens

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import org.koin.androidx.compose.koinViewModel
import pl.enteralt.core.DefaultValues
import pl.enteralt.hometestapp.R
import pl.enteralt.hometestapp.view.components.*
import pl.enteralt.hometestapp.view.model.ViewUserDetails
import pl.enteralt.hometestapp.view.viewmodel.DetailsUiState
import pl.enteralt.hometestapp.view.viewmodel.DetailsViewModel

@Composable
fun DetailsScreen(viewModel: DetailsViewModel = koinViewModel()) {

    val uiState by viewModel.uiState.collectAsState()

    Surface(modifier = Modifier.fillMaxSize()) {
        when (uiState) {
            is DetailsUiState.Error -> ErrorState(errorMessage = (uiState as DetailsUiState.Error).message)
            DetailsUiState.Loading -> Loader()
            is DetailsUiState.Success -> UserDetails(userDetails = (uiState as DetailsUiState.Success).data)
        }
    }
}

@Composable
private fun UserDetails(userDetails: ViewUserDetails) {

    val scrollState = rememberScrollState()

    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        BoxWithConstraints(modifier = Modifier.weight(1f)) {
            Surface {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .verticalScroll(scrollState),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    DetailsHeader(scrollState, userDetails.avatarUrl, this@BoxWithConstraints.maxHeight)
                    DetailsContent(userDetails = userDetails, this@BoxWithConstraints.maxHeight)
                }
            }
        }
    }
}

@Composable
private fun DetailsHeader(scrollState: ScrollState, imageUrl: String, containerHeight: Dp) {

    val offsetDp = with(LocalDensity.current) { (scrollState.value / 2).toDp() }

    AsyncImage(
        modifier = Modifier
            .heightIn(max = containerHeight / 2)
            .fillMaxWidth()
            .padding(top = offsetDp),
        model = imageUrl,
        contentDescription = DefaultValues.STRING,
        contentScale = ContentScale.Crop,
        placeholder = painterResource(id = android.R.drawable.ic_menu_gallery),
        error = painterResource(id = android.R.drawable.ic_menu_gallery)
    )
}

@Composable
private fun DetailsContent(userDetails: ViewUserDetails, containerHeight: Dp) {

    Column {
        val unknownStateText = stringResource(id = R.string.user_details_unknown)

        Spacer(modifier = Modifier.height(8.dp))
        DetailLogin(userDetails.login)
        DetailProperty(stringResource(R.string.user_details_name), userDetails.name ?: unknownStateText)
        DetailProperty(stringResource(R.string.user_details_company), userDetails.company ?: unknownStateText)
        DetailProperty(stringResource(R.string.user_details_location), userDetails.location ?: unknownStateText)
        DetailProperty(stringResource(R.string.user_details_email), userDetails.email ?: unknownStateText)
        Spacer(Modifier.height((containerHeight - 320.dp).coerceAtLeast(0.dp))) // spacer to show part of the fields list regardless of the device
    }
}

@Composable
private fun DetailLogin(userLogin: String) {

    Column(modifier = Modifier.padding(start = 16.dp, end = 16.dp, bottom = 16.dp)) {
        Text(
            text = userLogin,
            modifier = Modifier.baselineHeight(32.dp),
            style = MaterialTheme.typography.headlineMedium,
            fontWeight = FontWeight.Bold
        )
    }
}

@Composable
private fun DetailProperty(label: String, value: String) {

    Column(modifier = Modifier.padding(start = 16.dp, end = 16.dp, bottom = 16.dp)) {
        Divider()
        CompositionLocalProvider(LocalContentColor provides MaterialTheme.colorScheme.onSurfaceVariant) {
            Text(
                text = label,
                modifier = Modifier.baselineHeight(24.dp),
                style = MaterialTheme.typography.bodySmall,
                fontWeight = FontWeight.Medium
            )
        }
        Text(
            text = value,
            modifier = Modifier.baselineHeight(24.dp),
            style = MaterialTheme.typography.bodyLarge,
            fontWeight = FontWeight.Bold
        )
    }
}