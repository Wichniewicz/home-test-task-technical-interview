package pl.enteralt.hometestapp.view.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import org.koin.androidx.compose.koinViewModel
import pl.enteralt.core.DefaultValues
import pl.enteralt.hometestapp.R
import pl.enteralt.hometestapp.view.components.ErrorState
import pl.enteralt.hometestapp.view.components.Loader
import pl.enteralt.hometestapp.view.model.ViewUser
import pl.enteralt.hometestapp.view.viewmodel.ListUiState
import pl.enteralt.hometestapp.view.viewmodel.ListViewModel

@Composable
fun ListScreen(viewModel: ListViewModel = koinViewModel(), onNavigateToDetails: (String) -> Unit) {

    val uiState by viewModel.uiState.collectAsState()

    Surface(modifier = Modifier.fillMaxSize()) {
        when (uiState) {
            is ListUiState.Error -> ErrorState((uiState as ListUiState.Error).message)
            ListUiState.Loading -> Loader()
            is ListUiState.Success -> {
                val userList = (uiState as ListUiState.Success).data
                if (userList.isNotEmpty()) {
                    UserList(userList = userList, navigateToDetails = onNavigateToDetails)
                } else {
                    EmptyListState()
                }
            }
        }
    }
}

@Composable
fun UserList(userList: List<ViewUser>, navigateToDetails: (String) -> Unit) {
    LazyColumn {
        items(userList) { viewUser ->
            UserListItem(user = viewUser) {
                navigateToDetails.invoke(viewUser.login)
            }
        }
    }
}

@Composable
private fun UserListItem(user: ViewUser, onListItemClicked: () -> Unit) {
    Card(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth(),
        elevation = CardDefaults.cardElevation(2.dp),
        shape = RoundedCornerShape(corner = CornerSize(16.dp))
    ) {
        Row(modifier = Modifier.clickable { onListItemClicked() }) {
            UserImage(imageUrl = user.avatarUrl)
            Column(
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth()
                    .align(Alignment.CenterVertically)
            ) {
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight(),
                    text = user.login,
                    style = MaterialTheme.typography.headlineSmall,
                    fontWeight = FontWeight.Bold
                )
            }
        }
    }
}

@Composable
private fun EmptyListState() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Image(painter = painterResource(id = R.drawable.ic_list), contentDescription = DefaultValues.STRING)
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.CenterHorizontally),
            text = stringResource(id = R.string.empty_list),
            style = MaterialTheme.typography.headlineSmall,
            fontWeight = FontWeight.Medium,
            textAlign = TextAlign.Center
        )
    }
}

@Composable
private fun UserImage(imageUrl: String) {
    AsyncImage(
        modifier = Modifier
            .padding(8.dp)
            .size(84.dp)
            .clip(RoundedCornerShape(corner = CornerSize(16.dp))),
        model = imageUrl, contentDescription = DefaultValues.STRING,
        placeholder = painterResource(id = android.R.drawable.ic_menu_gallery),
        error = painterResource(id = android.R.drawable.ic_menu_gallery)
    )
}

@Preview
@Composable
fun ListScreenPreview() {
    ListScreen(onNavigateToDetails = {})
}