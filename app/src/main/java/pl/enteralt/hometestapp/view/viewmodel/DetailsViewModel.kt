package pl.enteralt.hometestapp.view.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import pl.enteralt.core.DefaultValues
import pl.enteralt.hometestapp.domain.repository.UsersRepository
import pl.enteralt.hometestapp.infrastructure.data.model.remote.ResultState
import pl.enteralt.hometestapp.view.model.ViewUserDetails
import pl.enteralt.hometestapp.view.model.mapping.ViewUsersMapper.toViewUserDetails

private const val TAG = "DetailsViewModel"

class DetailsViewModel(val userLogin: String, private val usersRepository: UsersRepository) : ViewModel() {

    private val _uiState: MutableStateFlow<DetailsUiState> = MutableStateFlow(DetailsUiState.Loading)
    val uiState = _uiState.asStateFlow()

    private fun fetchUserDetails() = viewModelScope.launch {

        usersRepository.getUserDetails(userLogin)
            .collectLatest { resultState ->
                when (resultState) {
                    is ResultState.Error -> {
                        Log.e(TAG, "getUserDetails -> Error state: ${resultState.cause.message}")
                        _uiState.value = DetailsUiState.Error(resultState.cause.message ?: DefaultValues.STRING)
                    }

                    is ResultState.Success -> _uiState.value = DetailsUiState.Success(resultState.data.toViewUserDetails())
                }
            }
    }

    init {
        fetchUserDetails()
    }
}

sealed interface DetailsUiState {
    data object Loading : DetailsUiState
    data class Success(val data: ViewUserDetails) : DetailsUiState
    data class Error(val message: String) : DetailsUiState
}