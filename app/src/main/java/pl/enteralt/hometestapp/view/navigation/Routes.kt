package pl.enteralt.hometestapp.view.navigation

enum class Routes {

    LIST,
    DETAILS
}