package pl.enteralt.hometestapp.view.model.mapping

import pl.enteralt.hometestapp.domain.model.User
import pl.enteralt.hometestapp.domain.model.UserDetails
import pl.enteralt.hometestapp.view.model.ViewUser
import pl.enteralt.hometestapp.view.model.ViewUserDetails

object ViewUsersMapper {

    private fun User.toViewUser() = ViewUser(login, avatarUrl)

    fun List<User>.toViewUserList() = map { it.toViewUser() }

    fun UserDetails.toViewUserDetails() = ViewUserDetails(login, avatarUrl, name, company, location, email)
}