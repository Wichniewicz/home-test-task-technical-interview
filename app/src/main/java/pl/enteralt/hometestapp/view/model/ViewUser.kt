package pl.enteralt.hometestapp.view.model

data class ViewUser(val login: String, val avatarUrl: String)