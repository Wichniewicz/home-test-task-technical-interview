package pl.enteralt.hometestapp.view.model

data class ViewUserDetails(
    val login: String,
    val avatarUrl: String,
    val name: String?,
    val company: String?,
    val location: String?,
    val email: String?
)