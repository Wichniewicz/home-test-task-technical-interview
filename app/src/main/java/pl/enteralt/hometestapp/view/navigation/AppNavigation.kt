package pl.enteralt.hometestapp.view.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.*
import androidx.navigation.navArgument
import org.koin.androidx.compose.koinViewModel
import org.koin.core.parameter.parametersOf
import pl.enteralt.core.DefaultValues
import pl.enteralt.hometestapp.view.screens.DetailsScreen
import pl.enteralt.hometestapp.view.screens.ListScreen
import pl.enteralt.hometestapp.view.viewmodel.DetailsViewModel

@Composable
fun AppNavigationGraph() {

    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = Routes.LIST.name) {

        composable(route = Routes.LIST.name) {
            ListScreen(onNavigateToDetails = { userLogin -> navController.navigate(route = "${Routes.DETAILS.name}/$userLogin") })
        }

        composable(
            route = "${Routes.DETAILS.name}/{userLogin}",
            arguments = listOf(navArgument("userLogin") { type = NavType.StringType })
        ) { backStackEntry ->
            koinViewModel<DetailsViewModel>(parameters = {
                parametersOf(
                    backStackEntry.arguments?.getString("userLogin") ?: DefaultValues.STRING
                )
            })
            DetailsScreen()
        }
    }
}