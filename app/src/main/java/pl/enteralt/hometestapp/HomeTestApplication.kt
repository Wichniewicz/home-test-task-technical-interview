package pl.enteralt.hometestapp

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import pl.enteralt.hometestapp.di.KoinModules

class HomeTestApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@HomeTestApplication)
            modules(KoinModules.all())
        }
    }
}