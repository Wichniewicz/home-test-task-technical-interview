object Dependencies {

    val coreKtx by lazy { "androidx.core:core-ktx:${Versions.CORE_KTX}" }
    val lifeCycleRuntimeKtx by lazy { "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.LIFECYCLE_RUNTIME_KTX}" }
    val activityCompose by lazy { "androidx.activity:activity-compose:${Versions.ACTIVITY_COMPOSE}" }
    val composeBom by lazy { "androidx.compose:compose-bom:${Versions.COMPOSE_BOM}" }
    val composeUi by lazy { "androidx.compose.ui:ui" }
    val composeUiGraphics by lazy { "androidx.compose.ui:ui-graphics" }
    val composeUiToolingPreview by lazy { "androidx.compose.ui:ui-tooling-preview" }
    val compposeUiTooling by lazy { "androidx.compose.ui:ui-tooling:${Versions.COMPOSE_UI_TOOLING}" }
    val composeMaterial3 by lazy { "androidx.compose.material3:material3" }
    val appCompat by lazy { "androidx.appcompat:appcompat:${Versions.APP_COMPAT}" }
    val material by lazy { "com.google.android.material:material:${Versions.MATERIAL}" }
    val koinCompose by lazy { "io.insert-koin:koin-androidx-compose:${Versions.KOIN}" }
    val koinComposeNavigation by lazy { "io.insert-koin:koin-androidx-compose-navigation:${Versions.KOIN}" }
    val okHttp by lazy { "com.squareup.okhttp3:okhttp:${Versions.OK_HTTP}" }
    val okHttpLoggingInterceptor by lazy { "com.squareup.okhttp3:logging-interceptor:${Versions.OK_HTTP}" }
    val retrofit by lazy { "com.squareup.retrofit2:retrofit:${Versions.RETROFIT}" }
    val retrofitMoshi by lazy { "com.squareup.retrofit2:converter-moshi:${Versions.RETROFIT}" }
    val retrofitGson by lazy { "com.squareup.retrofit2:converter-gson:${Versions.RETROFIT}" }
    val moshi by lazy { "com.squareup.moshi:moshi-kotlin:${Versions.MOSHI}" }
    val moshiCodeGen by lazy { "com.squareup.moshi:moshi-kotlin-codegen:${Versions.MOSHI}" }
    val coroutines by lazy { "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.COROUTINES}" }
    val splashScreen by lazy { "androidx.core:core-splashscreen:${Versions.SPLASH_SCREEN}" }
    val coil by lazy { "io.coil-kt:coil-compose:${Versions.COIL}" }
    val room by lazy { "androidx.room:room-runtime:${Versions.ROOM}" }
    val roomCompiler by lazy { "androidx.room:room-compiler:${Versions.ROOM}" }
    val roomCoroutines by lazy { "androidx.room:room-ktx:${Versions.ROOM}" }
}