object Versions {

    const val CORE_KTX = "1.12.0"
    const val LIFECYCLE_RUNTIME_KTX = "2.7.0"
    const val ACTIVITY_COMPOSE = "1.8.2"
    const val COMPOSE_BOM = "2023.08.00"
    const val COMPOSE_UI_TOOLING = "1.6.1"
    const val APP_COMPAT = "1.6.1"
    const val MATERIAL = "1.11.0"
    const val KOIN = "3.5.3"
    const val OK_HTTP = "4.12.0"
    const val RETROFIT = "2.9.0"
    const val MOSHI = "1.15.0"
    const val COROUTINES = "1.3.9"
    const val SPLASH_SCREEN = "1.0.0"
    const val COIL = "2.5.0"
    const val ROOM = "2.6.1"
}