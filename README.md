# Home test task - technical interview

Requirements:
- Project Objective: Create a functioning Android application that displays a list of GitHub users (referred to as items) and their details across two screens.
- Language: Kotlin
- Required Libraries: Jetpack Compose, Kotlin Coroutines
- Multilingual Support: English as default.
- Views: Design is open-ended; aesthetic appeal is not the primary focus, but correct display and tidy appearance are important.
- Data Source: The app should rely on API data, with an easily interchangeable data source.
- Architecture: The use of the MVVM or MVI pattern is recommended for this task.
- API: https://docs.github.com/en/rest

Suggested Libraries:
Coil, Koin, Retrofit, Room, or any other.

## Description

- All of required libraries where used: Jetpack Compose, Kotlin Coroutines, together with suggested ones: Coil, Koin, Retrofit, Room.
- MVVM design pattern was also used.
- Clean Architecture aproach was used, BUT with few changes (worth mentioning in my opinion): data layer was divided into local (database) and remote (API), so this project contains: API, database, domain and view layers. 
- There were no use cases (domain layer) introduced.
- Implemented simple caching via local data-source (Room database): first serves data from local data-source, then fetch it from remote data-source.

## TODO

Regardless of main objective done, there are things that still needs to be done: 
- tests needs to be written.
